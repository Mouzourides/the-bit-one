# the-bit-one

Use of Spring web flux, web sockets, react and Bitbuckets API to display a projects build status on the web.

To get your build on the radiator:
Go into your bitbucket project / Settings / Webhooks / Add webhook / Add url: `https://thebitone.co.uk/api/project-data` / Choose from a full list of triggers and select: `Build status created` and `Build status updated`
