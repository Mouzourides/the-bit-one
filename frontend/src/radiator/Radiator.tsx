/* tslint:disable:no-console */
import React, {Component} from "react";
import {IProjects} from "./Projects.state";
import "./Radiator.scss";
import {getProjectData} from "./Radiator.service";

export interface IRadiator {
    isLoading: boolean;
    projects: IProjects[];
    socket: any;
}

class Radiator extends Component<{}, IRadiator> {

    constructor(props: {}) {
        super(props);

        this.state = {
            isLoading: false,
            projects: [],
            socket: undefined,
        };
    }

    public async componentDidMount() {
        this.setState({isLoading: true});
        getProjectData()
            .then((data) => this.setState({projects: data, isLoading: false}));

        const url = location.href.includes("localhost")
            ? "ws://localhost:5000/event-emitter"
            : location.href.replace(/^https/, "wss") + "event-emitter";

        const socket = new WebSocket(url);
        this.setState({socket});

        socket.onopen = (event: any) => {
            console.log("Connected: ", event);
        };
        socket.onmessage = (event: any) => {
            const data: IProjects[] = JSON.parse(event.data);
            this.setState({projects: data, isLoading: false});
            console.log(event.data);
        };

        socket.onclose = () => {
            console.log("Closed");
        };

        setInterval(() => { socket.send("Ah ah ah ah staying alive, staying alive"); }, 30000);
    }

    public componentWillUnmount() {
        this.state.socket.close();
    }

    public render() {
        const {projects, isLoading} = this.state;

        return (
            <div className="App">
                <header className="App-header">
                    <div className="radiator">
                        {isLoading
                            ? <p>Loading...</p>
                            : projects.map((project) =>
                                <div className={"radiator-project " + project.buildStatus}>
                                    <h2>{project.name}</h2>
                                    <p>Build {project.buildStatus}</p>
                                </div>,
                            )}
                    </div>
                </header>
            </div>
        );
    }
}

export default Radiator;
