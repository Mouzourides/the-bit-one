import {IProjects} from "./Projects.state";

export async function getProjectData(): Promise<IProjects[]> {
    const response = await fetch("/api/project-data", {
        credentials: "include",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        method: "GET",
    });
    return await response.json();
}
