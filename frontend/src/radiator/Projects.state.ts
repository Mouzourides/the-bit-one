export interface IProjects {
    name: string;
    buildStatus: string;
    timestamp: string;
}
