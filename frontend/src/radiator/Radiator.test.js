import React from 'react';
import ReactDOM from 'react-dom';
import Radiator from "./Radiator";

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Radiator />, div);
  ReactDOM.unmountComponentAtNode(div);
});
