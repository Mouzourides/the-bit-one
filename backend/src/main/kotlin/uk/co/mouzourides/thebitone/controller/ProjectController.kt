package uk.co.mouzourides.thebitone.controller

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.EmitterProcessor
import reactor.core.publisher.Mono
import uk.co.mouzourides.thebitone.entities.Project
import java.time.LocalDateTime

@RestController
@RequestMapping("/api")
class ProjectController(private val emitterProcessor: EmitterProcessor<List<Project>>,
                        private val objectMapper: ObjectMapper) {

    var projects = emptyList<Project>()
    val logger: Logger = LoggerFactory.getLogger(ProjectController::class.java)

    @GetMapping("/project-data")
    @ResponseBody
    fun getAllProjects(): Mono<List<Project>> {
        return Mono.just(projects)
    }

    @PostMapping("/project-data")
    @ResponseBody
    fun addProject(@RequestBody newProject: String) {

        val projectName: String = objectMapper.readTree(newProject).get("repository").get("name").textValue()
        val commitStatus: String = objectMapper.readTree(newProject).get("commit_status").get("state").textValue()

        projects = if (projects.none { it.name == projectName }) projects + Project(projectName, commitStatus, LocalDateTime.now())
        else projects.map { if (it.name == projectName) it.copy(buildStatus = commitStatus, timestamp = LocalDateTime.now()) else it}

        logger.info("Sending message: $projects")
        emitterProcessor.onNext(projects)
    }
}
