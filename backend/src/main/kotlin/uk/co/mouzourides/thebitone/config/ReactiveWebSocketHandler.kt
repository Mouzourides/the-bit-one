package uk.co.mouzourides.thebitone.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketMessage
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.EmitterProcessor
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import uk.co.mouzourides.thebitone.entities.Project

@Component("ReactiveWebSocketHandler")
class ReactiveWebSocketHandler(emitterProcessor: EmitterProcessor<List<Project>>,
                               objectMapper: ObjectMapper) : WebSocketHandler {

    private val eventFlux: Flux<String> =
            emitterProcessor.map { objectMapper.writeValueAsString(it) }

    override fun handle(webSocketSession: WebSocketSession): Mono<Void> {
        return webSocketSession.send(
                eventFlux.map(webSocketSession::textMessage))
                .and(webSocketSession.receive()
                        .map(WebSocketMessage::getPayloadAsText)
                        .log())
    }
}
