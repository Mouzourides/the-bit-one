package uk.co.mouzourides.thebitone.entities

import java.time.LocalDateTime

data class Project(val name: String, val buildStatus: String, val timestamp: LocalDateTime)
