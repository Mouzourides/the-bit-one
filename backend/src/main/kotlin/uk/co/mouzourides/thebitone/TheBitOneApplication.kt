package uk.co.mouzourides.thebitone

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.EmitterProcessor
import reactor.core.publisher.Mono
import uk.co.mouzourides.thebitone.entities.Project

@SpringBootApplication
class TheBitOneApplication {
    @Bean
    fun emitterProcessor(): EmitterProcessor<List<Project>> {
        // Auto cancel set to false
        return EmitterProcessor.create(false)
    }

    @Bean
    fun objectMapper(): ObjectMapper {
        return ObjectMapper()
    }
}

fun main(args: Array<String>) {
    runApplication<TheBitOneApplication>(*args)
}

@Component
class RedirectWebFilter : WebFilter {
    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        return if (exchange.request.uri.path == "/") {
            chain.filter(exchange.mutate().request(exchange.request.mutate().path("/index.html").build()).build())
        } else chain.filter(exchange)
    }
}
